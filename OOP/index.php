<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$animal = new Animal("Shaun");
echo "Nama Binatang : " . $animal->name . "<br>";
echo "Jumlah Kaki : " . $animal->legs . "<br>";
echo "Cold Blooded : " . $animal->cold_blooded . "<br><br>";

$kodok = new frog("Buduk");
echo "Nama Binatang : " . $kodok->name . "<br>";
echo "Jumlah Kaki : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
$kodok->gerak("hop hop <br><br>");

$ape = new Ape("Kera Sakti");
echo "Nama Binatang : " . $ape->name . "<br>";
echo "Jumlah Kaki : " . $ape->legs . "<br>";
echo "Cold Blooded : " . $ape->cold_blooded . "<br>";
echo $ape->suara("Auooo <br><br>");


?>
