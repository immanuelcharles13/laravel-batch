<?php

use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', [HomeController::class, 'dashboard']);
Route::get('/daftar', [AuthController::class, 'daftar']);
Route::post('/home', [AuthController::class, 'home']);


Route::get('/data-table', function() {
    return view('page.data-table');
});
Route::get('/table', function() {
    return view('page.table');
});

// Create Data mengarah ke form cast
Route::get('/cast/create',[CastController::class, 'create']);
Route::post('/cast',[CastController::class, 'store']);
// Read data
Route::get('/cast',[CastController::class, 'index']);
Route::get('/cast/{id}',[CastController::class, 'show']);