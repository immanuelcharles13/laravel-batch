@extends('layouts.master')
@section('title')
    Halaman Tambah Cast
@endsection
@section('content')
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">First</th>
        <th scope="col">Action</th>
        
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=> $item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$item->name}}</td>
            <td>
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            </td>
        </tr>   
        @empty
            <h1>Data Cast Kosong</h1>
        @endforelse
      
    </tbody>
  </table>
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>

@endsection